#!/usr/bin/python

import sys
import json

filtered_list = []

# Print help if necessary
if len(sys.argv) < 2:
  sys.exit("Usage: {} file_to_parse.json".format(sys.argv[0]))

# Load Openweathermap city data file
with open(sys.argv[1], 'r') as infile:
  indata = json.load(infile)

# Generate a new dictionary filter by 'US' country code
indata_us = [j for j in indata if j['country'] == 'US']

# Iterate over the dictionary to write a new line of CSV
for j in indata_us:
      csv_string=(", ".join([str(j['id']), j['name'], j['state']]))
      filtered_list.append(csv_string)

# Write to output.csv appending a newline at end of each string
with open("outfile.csv", 'w') as outfile:
  outfile.writelines("\n".join(filtered_list))
