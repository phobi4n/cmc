#!/usr/bin/python
# CMC Media Center Support
# Copyright (C) 2021  Mark Whittaker
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import gi
import sys
import csv

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from pathlib import Path

# Your own Openweathermap API key
API_KEY = "eae76ca13c42d3708964108793349768"

states_list = []
locations = []
city_dict = {}

class WeatherWindow(Gtk.Window):
  def __init__(self, unique_states, locations):
    Gtk.Window.__init__(self, title='Location for Weather Data')
    self.set_border_width(16)
    self.set_default_size(360,200)
    self.id_dict = {}
    
    states_store = Gtk.ListStore(str)
    states_store.append(["Select..."])

    for place in unique_states:
      states_store.append([place])
    
    states_combo = Gtk.ComboBox.new_with_model(states_store)
    renderer_text = Gtk.CellRendererText()
    states_combo.pack_start(renderer_text, True)
    states_combo.add_attribute(renderer_text, "text", 0)
    states_combo.props.id_column = 0
    states_combo.set_active_id("Select...")
    
    towns_store = Gtk.ListStore(str)
    towns_store.append(["Select..."])

    towns_combo = Gtk.ComboBox.new_with_model(towns_store)
    renderer_text2 = Gtk.CellRendererText()
    towns_combo.pack_start(renderer_text2, True)
    towns_combo.add_attribute(renderer_text2, "text",0)
    towns_combo.props.id_column = 0
    towns_combo.set_active_id("Select...")
    towns_combo.set_sensitive(False)
    
    # Actions on combos changed
    states_combo.connect("changed", self.on_states_changed, towns_store, towns_combo)
    
    # Begin layout
    box_outer = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=16)
    self.add(box_outer)
    
    listbox = Gtk.ListBox()
    listbox.set_selection_mode(Gtk.SelectionMode.NONE)
    box_outer.pack_start(listbox, True, True, 0)

    row = Gtk.ListBoxRow()
    hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=16)
    row.add(hbox)
    label = Gtk.Label(label="Select State", xalign=0)
    hbox.pack_start(label, True, True, 0)
    hbox.pack_start(states_combo, False, True, 0)
    listbox.add(row)
    
    row = Gtk.ListBoxRow()
    hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=16)
    row.add(hbox)
    label = Gtk.Label(label="Select Town", xalign=0)
    hbox.pack_start(label, True, True, 0)
    hbox.pack_start(towns_combo, False, True, 0)
    listbox.add(row)
    
    hbox = Gtk.Box(spacing=16)
    box_outer.add(hbox)
    
    button = Gtk.Button.new_with_mnemonic("_Save")
    button.connect("clicked", self.save_location, towns_combo, self.id_dict)
    hbox.pack_start(button, True, True, 0)

    button = Gtk.Button.new_with_mnemonic("_Cancel")
    button.connect("clicked", self.just_leaving)
    hbox.pack_start(button, True, True, 0)
    
  def on_states_changed(self, combo, towns_store, towns_combo):
    combo_iter = combo.get_active_iter()
    if combo_iter is not None:
      model = combo.get_model()
      state = model[combo_iter][:2]
    else:
      return
    towns_store.clear()
    city_dict = {}
    for locale in locations:
      if locale[2] == state[0]:
        city_dict[locale[1]] = locale[0]
    flag = True
    for i in sorted(city_dict):
      towns_store.append([i])
      if flag:
        first_town = i
        flag = False
    towns_combo.set_active_id(first_town)
    towns_combo.set_sensitive(True)
    self.id_dict.update(city_dict)
  
  def on_towns_changed(self, combo, towns_combo, id_dict):
    combo_iter = towns_combo.get_active_iter()
    if combo_iter is not None:
      model = towns_combo.get_model()
      state = model[combo_iter][:2]
    else:
      return
  
  def save_location(self, button, towns_combo, id_dict):
    combo_iter = towns_combo.get_active_iter()
    if combo_iter is not None:
      model = towns_combo.get_model()
      state = model[combo_iter][:2]
      print(id_dict[state[0]])
      with open('openweathermapAPI', 'w') as outfile:
        outfile.write(
          "https://api.openweathermap.org/data/2.5/weather?id={}&appid={}&units=metric&mode=xml".format(
            id_dict[state[0]], API_KEY))
      with open('openweathermapURL', 'w') as outfile:
        outfile.write("https://openweathermap.org/city/{}".format(id_dict[state[0]]))
      Gtk.main_quit()
    else:
      return
  
  def just_leaving(self, button):
    Gtk.main_quit()


# Begin here--------
with open('outfile.csv', 'r') as infile:
  csv_reader = csv.reader(infile, delimiter=',')
  
  for row in csv_reader:
    states_list.append(row[2].strip())
    ident, city, state = row[0].strip(), row[1].strip(), row[2].strip()
    locations.append([ident, city, state])

unique_states = list(set(states_list))
unique_states.sort()
unique_states.remove('00')
unique_states.remove('')

# Create and show our window
win = WeatherWindow(unique_states, locations)
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
