#!/bin/bash

# CMC Media Center Support
# Copyright (C) 2021  Mark Whittaker
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Clean any stray token files
if [ -f ./.adult ]; then
    rm -f ./.adult
fi

if [ -f ./.pidfile ]; then
    rm -f ./.pidfile
fi

if [ -f ./.pidfile-weather ]; then
    rm -f ./.pidfile-weather
fi

# Murder any panel
killall tint2

# Set background
hsetroot -fill wallpaper.jpg &

# Start panel, add local directory to PATH so it can find the scripts
PATH=$PWD:$PATH tint2 -c tint2rc &

# Use PWD so we can run this anywhere
ARGS="--no-buttons --undecorated --html --browser --borders=0 --tab-borders=0 \
      --width=165 --height=1050 --posx=0 --posy=0 --gtkrc=$PWD/gtk.css \
      --uri=file://$PWD/cmc_start.html --uri-handler=\"$PWD/wrapper.sh\" "

yad $ARGS
