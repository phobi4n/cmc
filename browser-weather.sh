#!/bin/bash

# Test if the weather window is already open. If so, close it.

if [ -f .weather ]; then
  xargs kill < .weather
  rm -f .weather
else
  URL=$(cat Weather/openweathermapURL)

  # Record process ID then exec chromium which will inherit this PID.
  echo $$ > .weather ; exec yad --no-buttons --undecorated --html\
  --uri="$URL" --width=940 --height=890 --posx=980 --posy=160
fi

exit 0
