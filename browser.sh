#!/bin/bash

# Record process ID then exec chromium which will inherit this PID.
exec chromium --app="$1" --window-size=940,770 --window-position=360,60
