#!/bin/bash

# Test if the weather window is already open. If so, close it.

if [ -f ".calendar" ]; then
  xargs kill < .calendar
  rm -f .calendar
else
  # Record process ID then exec chromium which will inherit this PID.
  echo $$ > .calendar ; exec /usr/bin/osmo
fi

exit 0
