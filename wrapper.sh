#!/bin/bash

#     CMC Media Center Support
#     Copyright (C) 2021  Mark Whittaker
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Debug
echo "Wrapper was called with $1"

# Return 0 for yad to handle, 1 for this script to handle
RETVAL=1

# Example password - this should be stored somewhere else
SECRET="adult"

# After calling browser, wait 5 seconds then use xdotool to reload the page
reload_avira () {
    sleep 5
    xdotool search --onlyvisible --class chromium windowfocus key F5
    }
        
# switch on contents of $1
case $1 in
        
    # If the address contains adult we want a password prompt
    *pr0n*)
        # This is more complicated than it needs to be.
        # If we want yad to open a new page it calls this script again.
        
        # Test if this is our second pass
        if [ -f ./.adult ]; then
            rm -f ./.adult
            RETVAL=0
        else
            # Show password prompt
            ANSWER=$(yad --entry --entry-label=Password --hide-text --title="Enter Password")
            # If correct, set a token for second pass
            if [ "$ANSWER" == "$SECRET" ]; then
                touch ./.adult
                RETVAL=0
            fi
        fi
        ;;
    
    *cmc*)
        # Return 0 so yad loads this as a new page
        RETVAL=0
        ;;
        
    https:*)
        # Check for pidfile from browser launch then test its actually
        # a chromium process. If so, kill the process and remove pidfile.
        
        #if [ -f ./.pidfile ]; then
            #PD=$(cat ./.pidfile)
            #PIDS=$(pidof chromium)
            
            #if [[ "$PIDS" == *"$PD"* ]]; then
                #kill "$PD"
            #fi
            
            #rm -f ./.pidfile
        #fi
        
        killall chromium
        
        # Spawn another shell that will write process ID then exec chromium
        ./browser.sh "$1" &
        reload_avira &
        ;;
    
    *logout)
        SESSION=$(loginctl session-status | head -n1 | awk '{print $1}')
        loginctl terminate-session "$SESSION"
        ;;
        
    *reboot)
        reboot
        ;;
    
    *shutdown)
        poweroff
        ;;
    
    *userguide)
        yad --text-info --filename=LICENSE --text="<big><b> Media Center Control - User Guide</b></big>" --gtkrc=gtk-info.css --button=yad-close --width 640 --height 480 --title "User Guide" &
        ;;

    *)
        ;;
esac

echo "Returning $RETVAL"
exit $RETVAL
