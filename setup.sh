#!/bin/bash

INFO="\033[1;33;44m"
END="\033[0m"
OPENBOX="$HOME/.config/openbox"

echo -e "${INFO}-- Copying custom font ${END}"
cp -v font/* ~/.fonts

echo -e "${INFO}-- Copying custom Openbox config ${END}"

if [ ! -d $OPENBOX ] ; then
  mkdir -pv $OPENBOX
fi

cp -v rc.xml $OPENBOX

echo -e "${INFO}-- Creating autostart ${END}"
echo "$PWD/cmc_autostart.sh &" > "$OPENBOX/autostart"

